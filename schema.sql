CREATE TABLE mm_account (
       id          INTEGER PRIMARY KEY,
       name        VARCHAR NOT NULL,
       description VARCHAR NULL,
       color       VARCHAR NOT NULL
);

CREATE TABLE mm_category (
       id          INTEGER PRIMARY KEY,
       name        VARCHAR NOT NULL,
       description VARCHAR NULL,
       color       VARCHAR NOT NULL
);

CREATE TABLE mm_transaction (
       id          INTEGER PRIMARY KEY,
       date        TIMESTAMP NOT NULL,
       description VARCHAR NULL,
       account_id  INTEGER NOT NULL REFERENCES mm_account (id)
);

CREATE TABLE mm_transaction_category (
       transaction_id INTEGER NOT NULL REFERENCES mm_transaction (id),
       category_id    INTEGER NOT NULL REFERENCES mm_transaction_category (id),
       amount         INTEGER NOT NULL,
       PRIMARY KEY (transaction_id, category_id)
);

CREATE TABLE mm_transaction_comment (
       transaction_id INTEGER NOT NULL REFERENCES mm_transaction (id),
       date           TIMESTAMP NOT NULL,
       text           VARCHAR NOT NULL
);

