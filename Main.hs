{-# LANGUAGE OverloadedStrings #-}

module Main (main) where

import Model
import Persistence

import qualified Web.Scotty as S
import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import Control.Monad.Reader
import Database.HDBC.Sqlite3
import Database.HDBC
import Data.Aeson
import Network.HTTP.Types.Status
import Web.PathPieces
import Data.Pool

pluralize :: String -> String
pluralize "" = ""
pluralize x = case last x of
  'y' -> init x ++ "ies"
  's' -> x
  _ -> x ++ "s"

resource :: (FromJSON a, ToJSON a, Entity a) => String -> Pool Connection -> (Key a -> Key a) -> (KeyValue a -> KeyValue a) -> S.ScottyM ()
resource url pool kconv econv = do
  S.post (S.capture $ pluralize url) $ do
    newEntity <- S.jsonData
    Key newKey <- liftM kconv $ rundb pool (create newEntity)
    S.status status201
    S.setHeader "Location" $ L.fromChunks [T.pack url, "/", toPathPiece newKey]
  S.get (S.capture $ url ++ "/:id") $ do
    idParam <- S.param "id"
    found <- rundb pool (retrieveSingle (Key idParam))
    case found of
      Just found' -> S.json (econv found')
      _ -> S.html "not found"
  S.get (S.capture $ pluralize url) $ do
    objects <- rundb pool retrieveAll
    S.json $ map econv objects
  S.delete (S.capture $ url ++ "/:id") $ do
    idParam <- S.param "id"
    idParam' <- return $ kconv (Key idParam)
    rundb pool (delete idParam')
    S.status status204
    S.setHeader "Location" $ L.pack $ pluralize url
  S.put (S.capture $ url ++ "/:id") $ do
    idParam <- S.param "id"
    modified <- S.jsonData
    k@(Key k') <- return $ kconv (Key idParam)
    rundb pool $ update k modified
    S.status status204
    S.setHeader "Location" $ L.fromChunks [T.pack url, "/", toPathPiece k']

accountkey :: Key Account -> Key Account
accountkey = id

accountkeyvalue :: KeyValue Account -> KeyValue Account
accountkeyvalue = id

categorykey :: Key Category -> Key Category
categorykey = id

categorykeyvalue :: KeyValue Category -> KeyValue Category
categorykeyvalue = id

transactionkey :: Key Transaction -> Key Transaction
transactionkey = id

transactionkeyvalue :: KeyValue Transaction -> KeyValue Transaction
transactionkeyvalue = id

main :: IO ()
main = do
  pool <- createPool (connectSqlite3 "moneymoney.sqlite3") disconnect 1 10 5
  S.scotty 3000 $ do
    resource "/account" pool accountkey accountkeyvalue
    resource "/category" pool categorykey categorykeyvalue
    resource "/transaction" pool transactionkey transactionkeyvalue
