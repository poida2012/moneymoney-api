{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}

module Persistence (rundb, Entity(..)) where

import Model
import Database.HDBC hiding (executeMany)
import Database.HDBC.Sqlite3
import Control.Monad.Reader
import qualified Data.Text as T
import qualified Data.ByteString.Char8 as B
import Data.Maybe
import Data.Convertible.Base
import Data.List
import Data.List.NonEmpty hiding (map)
import Data.Time.Clock
import Data.Fixed
import Data.Aeson
import Data.Pool

data AppState = AppState { dbcon :: (Pool Connection) }
type App a = ReaderT AppState IO a

rundb db action = lift $ runReaderT action (AppState{dbcon=db})

executeSingle :: Connection -> String -> [SqlValue] -> IO (Maybe [SqlValue])
executeSingle db sql args =
  do stmt <- prepare db sql
     count <- execute stmt args
     fetchRow stmt

executeMany :: Connection -> String -> [SqlValue] -> IO [[SqlValue]]
executeMany db sql args =
  do stmt <- prepare db sql
     execute stmt args
     fetchAllRows stmt

getLastId :: Connection -> IO (Maybe Int)
getLastId db = do
  result <- executeSingle db "select last_insert_rowid()" []
  case result of
    Just (id:_) -> return (Just $ fromSql id)
    _ -> return Nothing

newtype Sql a = Sql String

class Entity a where
  create :: a -> App (Key a)
  retrieveSingle :: Key a -> App (Maybe (KeyValue a))
  retrieveAll :: App [KeyValue a]
  update :: Key a -> a -> App ()
  delete :: Key a -> App ()

withdb :: (Connection -> IO a) -> App a
withdb action = do
  pool <- asks dbcon
  lift $ withResource pool (\db -> action db)

createEntity' :: (Entity a) => Sql a -> (a -> [SqlValue]) -> a -> Connection -> IO (Key a)
createEntity' (Sql sql) marshal entity db = do
  executeSingle db sql (marshal entity)
  commit db
  id <- getLastId db
  case id of
    Just id' -> return (Key id')

kv :: (Entity a) => Int -> a -> KeyValue a
kv k v = KeyValue { key=(Key k), value=v }

getEntity :: (Entity a) => Sql a -> ([SqlValue] -> Maybe (KeyValue a)) -> Key a -> Connection -> IO (Maybe (KeyValue a))
getEntity (Sql sql) parse (Key id) db = do
  result <- executeSingle db sql [toSql id]
  return (result >>= parse)

getEntities :: (Entity a) => Sql a -> ([SqlValue] -> Maybe (KeyValue a)) -> Connection -> IO [KeyValue a]
getEntities (Sql sql) parse db = do
  result <- executeMany db sql []
  return (catMaybes $ map parse result)

updateEntity :: (Entity a) => Sql a -> (a -> [SqlValue]) -> Key a -> a -> Connection -> IO ()
updateEntity (Sql sql) marshal (Key id) entity db = do
  executeSingle db sql ((marshal entity)++[toSql id])
  commit db
        
deleteEntity :: (Entity a) => Sql a -> Key a -> Connection -> IO ()
deleteEntity (Sql sql) (Key id) db = do
  executeSingle db sql [toSql id]
  commit db

-- Account

accountFromSql (i:n:d:c:[]) = Just $ kv (fromSql i) (Account (fromSql n) (fromSql d) (fromSql c))
accountFromSql _ = Nothing

accountToSql (Account n d c) = [toSql n, toSql d, toSql c]

instance Entity Account where
  create e = withdb $ createEntity' (Sql "insert into mm_account (name, description, color) values (?,?,?)") accountToSql e
  retrieveSingle k = withdb $ getEntity (Sql "select id, name, description, color from mm_account where id=?") accountFromSql k
  retrieveAll = withdb $ getEntities (Sql "select id, name, description, color from mm_account") accountFromSql
  update k e = withdb $ updateEntity (Sql "update mm_account set name=?, description=?, color=? where id=?") accountToSql k e
  delete k = withdb $ deleteEntity (Sql "delete from mm_account where id=?") k
            
-- Category

categoryFromSql (i:n:d:c:[]) = Just $ kv (fromSql i) (Category (fromSql n) (fromSql d) (fromSql c))
categoryFromSql _ = Nothing

categoryToSql (Category n d c) = [toSql n, toSql d, toSql c]

instance Entity Category where
  create e = withdb $ (createEntity' (Sql "insert into mm_category (name, description, color) values (?,?,?)") categoryToSql e)
  retrieveSingle k = withdb $ (getEntity (Sql "select id, name, description, color from mm_category where id=?") categoryFromSql k)
  retrieveAll = withdb $ getEntities (Sql "select id, name, description, color from mm_category") categoryFromSql
  update k e = withdb $ updateEntity (Sql "update mm_category set name=?, description=?, color=? where id=?") categoryToSql k e
  delete k = withdb $ deleteEntity (Sql "delete from mm_category where id=?") k
            
-- Transaction

insertTran :: Connection -> Transaction -> IO ()
insertTran db (Transaction dat des (Key acc) _ _) = do
  result <- executeSingle db "insert into mm_transaction (date, description, account_id) values (?, ?, ?)" [toSql dat, toSql des, toSql acc]
  return ()
  
insertTranCat :: Connection -> Int -> TransactionCategory -> IO ()
insertTranCat db t_id (TransactionCategory (Key c_id) amount) = do
  executeSingle db "insert into mm_transaction_category (transaction_id, category_id, amount) values (?, ?, ?)" [toSql t_id, toSql c_id, toSql amount]
  return ()

insertTranCom :: Connection -> Int -> TransactionComment -> IO ()
insertTranCom db t_id (TransactionComment date text) = do
  executeSingle db "insert into mm_transaction_comment (transaction_id, date, text) values (?, ?, ?)" [toSql t_id, toSql date, toSql text]
  return ()

parseTran :: [SqlValue] -> Maybe (KeyValue Transaction)
parseTran (i:da:de:a:_) = Just $ kv (fromSql i) (Transaction (fromSql da) (fromSql de) (Key (fromSql a)) undefined undefined)
parseTran _ = Nothing                          

parseTranCat :: [[SqlValue]] -> [TransactionCategory]
parseTranCat = map parse
  where parse (c:a:_) = (TransactionCategory (Key (fromSql c)) (fromSql a))

parseTranCom :: [[SqlValue]] -> [TransactionComment]
parseTranCom = map parse
  where parse (d:t:_) = (TransactionComment (fromSql d) (fromSql t))

createTransaction :: Transaction -> Connection -> IO (Key Transaction)
createTransaction t@(Transaction _ _ _ cat com) db = do
  insertTran db t
  tid <- getLastId db
  case tid of
    Just tid' -> do mapM_ (insertTranCat db tid') (toList cat)
                    mapM_ (insertTranCom db tid') com
                    commit db
                    return (Key tid')
    _ -> do rollback db
            fail "unable to create transaction"

retrieveTransaction :: Key Transaction -> Connection -> IO (Maybe (KeyValue Transaction))
retrieveTransaction (Key id) db = do
    t <- executeSingle db "select id, date, description, account_id from mm_transaction where id=?" [toSql id]
    t' <- return $ (t >>= parseTran)
    case t' of
      (Just (KeyValue _ (Transaction da de a _ _))) -> do
        cas <- executeMany db "select category_id, amount from mm_transaction_category where transaction_id=?" [toSql id]
        cas' <- return $ parseTranCat cas
        cos <- executeMany db "select date, text from mm_transaction_comment where transaction_id=?" [toSql id]
        cos' <- return $ parseTranCom cos
        return (Just (kv id (Transaction da de a (fromList cas') cos')))
      _ -> return Nothing  
            
retrieveTransactions :: Connection -> IO [KeyValue Transaction]
retrieveTransactions db = do
  ts <- executeMany db "select id, date, description, account_id from mm_transaction" []
  do all <- mapM load ts
     return $ catMaybes $ all
  where load t =  do
          t' <- return $ parseTran t
          case t' of
            (Just (KeyValue (Key id) (Transaction da de a _ _))) -> do
              cas <- executeMany db "select category_id, amount from mm_transaction_category where transaction_id=?" [toSql id]
              cas' <- return $ parseTranCat cas
              cos <- executeMany db "select date, text from mm_transaction_comment where transaction_id=?" [toSql id]
              cos' <- return $ parseTranCom cos
              return (Just (kv id (Transaction da de a (fromList cas') cos')))
            _ -> return Nothing

updateTransaction :: Key Transaction -> Transaction -> Connection -> IO ()
updateTransaction (Key id) (Transaction da de (Key a) cat com) db = do
  executeSingle db "update mm_transaction set date=?, description=?, account_id=? where id=?" [toSql da, toSql de, toSql a, toSql id]
  executeSingle db "delete from mm_transaction_category where transaction_id=?" [toSql id]
  executeSingle db "delete from mm_transaction_comment where transaction_id=?" [toSql id]
  mapM_ (insertTranCat db id) (toList cat)
  mapM_ (insertTranCom db id) com
  commit db
  return ()

deleteTransaction :: Key Transaction -> Connection -> IO ()
deleteTransaction (Key id) db = do
  executeSingle db "delete from mm_transaction_category where transaction_id=?" [toSql id]
  executeSingle db "delete from mm_transaction_comment where transaction_id=?" [toSql id]
  executeSingle db "delete from mm_transaction where id=?" [toSql id]
  commit db
  return ()

instance Entity Transaction where
  create t = withdb $ (createTransaction t)
  retrieveSingle k = withdb $ (retrieveTransaction k)
  retrieveAll = withdb retrieveTransactions
  update k e = withdb (updateTransaction k e)
  delete k = withdb (deleteTransaction k)

instance Convertible (Fixed E2) SqlValue where
  safeConvert c = safeConvert $ ((truncate $ (c :: Fixed E2) * 100) :: Int)

instance Convertible SqlValue (Fixed E2) where
  safeConvert (SqlInt64 x) = return (((fromIntegral x) :: Centi) / 100)
  safeConvert (SqlByteString x) = return (((fromIntegral (read (B.unpack x))) :: Centi) / 100)
