{-# LANGUAGE OverloadedStrings #-}

module Model where

import Data.Time
import qualified Data.Text as T
import qualified Data.ByteString.Lazy.Char8 as L
import Data.Aeson
import Control.Applicative
import Control.Monad
import Data.Fixed
import Data.List.NonEmpty (NonEmpty ((:|)))
import qualified Data.HashMap.Strict as HM
import qualified Data.Vector as V

newtype Key a = Key Int
                deriving Show

instance ToJSON (Key a) where
  toJSON (Key x) = toJSON x

instance FromJSON (Key a) where
  parseJSON (Number n) = return (Key (truncate n))

data KeyValue a = KeyValue { key :: Key a, value :: a }

instance (ToJSON a) => ToJSON (KeyValue a) where
  toJSON x = case (toJSON (value x)) of
    (Object hashmap) -> (Object (HM.insert "id" (toJSON $ key x) hashmap))

instance (ToJSON a) => ToJSON (NonEmpty a) where
  toJSON (a :| []) = toJSON [a]
  toJSON (a :| as) = toJSON (a:as)

instance (FromJSON a) => FromJSON (NonEmpty a) where
  parseJSON (Array a) = do l <- mapM parseJSON (V.toList a)
                           case l of
                             [] -> mzero
                             (x:xs) -> return (x :| xs)

-- Account

data Account = Account { accountName :: String
                       , accountDescription :: Maybe T.Text
                       , accountColor :: String }
               deriving Show

instance ToJSON Account where
  toJSON (Account name description color) = object ["name" .= name, "description" .= description, "color" .= color]

instance FromJSON Account where
  parseJSON (Object a) = Account <$> a .: "name" <*> a .: "description" <*> a .: "color"

-- Category

data Category = Category { categoryName :: String
                         , categoryDescription :: Maybe T.Text
                         , categoryColor :: String }
                deriving Show

instance ToJSON Category where
  toJSON (Category name description color) = object ["name" .= name, "description" .= description, "color" .= color]

instance FromJSON Category where
  parseJSON (Object c) = Category <$> c .: "name" <*> c .: "description" <*> c .: "color"

-- Transaction Comment

data TransactionComment = TransactionComment { trancomDate :: UTCTime
                                             , trancomText :: T.Text }
                        deriving Show

instance ToJSON TransactionComment where
  toJSON (TransactionComment date text) = object ["date" .= date, "text" .= text]

instance FromJSON TransactionComment where
  parseJSON (Object c) = TransactionComment <$> c .: "date" <*> c .: "text"

-- Transaction Category

data TransactionCategory = TransactionCategory { trancatCategoryId :: Key Category
                                               , trancatAmount :: Centi }
                         deriving Show

instance ToJSON TransactionCategory where
  toJSON (TransactionCategory cid a) = object [ "categoryId" .= cid, "amount" .= a ]

instance FromJSON TransactionCategory where
  parseJSON (Object tc) = TransactionCategory <$> tc .: "categoryId" <*> tc .: "amount"

-- Transaction

data Transaction = Transaction { transactionDate :: UTCTime
                               , transactionDescription :: Maybe T.Text
                               , transactionAccountId :: Key Account
                               , transactionCategories :: NonEmpty TransactionCategory
                               , transactionComments :: [TransactionComment] }
                 deriving Show

instance ToJSON Transaction where
  toJSON (Transaction date description accountId categories comments) = object [ "date" .= date, "description" .= description, "accountId" .= accountId, "categories" .= categories, "comments" .= comments]

instance FromJSON Transaction where
  parseJSON (Object t) = Transaction <$> t .: "date" <*> t .: "description" <*> t .: "accountId" <*> t .: "categories" <*> t .: "comments"

